# PDF - PDF-Generator

## Features

* Generiert ein PDF-Dokument aus einem Node im PDF/A-Standard
* Aktivierung erfolgt am Inhaltstyp

## Berechtigungen

* access pdf version: Ansicht PDF-Version des Inhalts

## Vorraussetzungen

* [Drupal 8](https://www.drupal.org/8)
* [phpoffice/phpword](https://phpword.readthedocs.io/)
* [LibreOffice](https://gitlab.com/lwb-web/drupal/docker#drupalpdf)
