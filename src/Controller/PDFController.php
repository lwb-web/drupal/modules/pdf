<?php

namespace Drupal\pdf\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\pdf\PdfGenerator;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Contains the BaseController controller.
 */
class PDFController extends ControllerBase {

  /**
   * Create and Download PDF.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Current Node.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   *   Returns a redirect to the node of the currently closed process.
   *
   * @throws \PhpOffice\PhpWord\Exception\Exception
   */
  public function createPdf(Node $node): BinaryFileResponse {
    $headers = [
      'Content-Description' => 'File Transfer',
      'Content-Disposition' => 'attachment; filename="' . $node->id() . '.pdf"',
      'Content-Type' => 'application/pdf',
      'Content-Transfer-Encoding' => 'binary',
      'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
      'Expires' => '0',
    ];

    return new BinaryFileResponse(PdfGenerator::generate($node), 200, $headers, TRUE);
  }

}
