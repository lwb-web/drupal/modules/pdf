<?php

namespace Drupal\pdf;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;

/**
 * Class for PdfExtractor.
 */
class PdfExtractor {

  /**
   * Extracts data of a node.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Current Node.
   *
   * @return array
   *   Extracted data.
   */
  public function extract(Node $node) {
    $output = [
      'label' => NodeType::load($node->bundle())->label() . ': ' . $node->getTitle(),
      'values' => $this->extractGroups($node),
    ];

    return $output;
  }

  /**
   * Extracts the groups of a node.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Current Node.
   *
   * @return array
   *   Extracted groups.
   */
  private function extractGroups(Node $node) {
    $output = [];
    $typeManager = \Drupal::entityTypeManager();

    $builder = $typeManager->getViewBuilder('node');
    $display = $typeManager
      ->getStorage('entity_view_display')
      ->load('node.' . $node->bundle() . '.default');

    $displayFields = $display->getComponents();
    $view = $builder->view($node, 'full');
    $elements = $builder->build($view);

    foreach ($elements['#fieldgroups'] as $group) {
      $output_group = $this->extractFields($node, $group->children, $displayFields);

      if (count($output_group)) {
        $output[] = ['label' => $group->label, 'values' => $output_group];
      }
    }

    return $output;
  }

  /**
   * Extracts the fields of groups.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Current Node.
   * @param array $fields
   *   Array of field names.
   * @param array $displayFields
   *   An entity object array.
   *
   * @return array
   *   Extracted fields.
   */
  private function extractFields(Node $node, array $fields, array $displayFields) {
    $output = [];

    foreach ($fields as $field_name) {
      $output_field = $this->extractField($node, $field_name, $displayFields);

      if (count($output_field)) {
        $output[] = $output_field;
      }
    }

    return $output;
  }

  /**
   * Extracts a field.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Current Node.
   * @param string $field_name
   *   Name of field.
   * @param array $displayFields
   *   An entity object array.
   *
   * @return array
   *   Extracted field data.
   */
  private function extractField(Node $node, string $field_name, array $displayFields) {
    $output = [];
    if ($node->hasField($field_name)) {
      $output_values = [];
      $fieldDefinition = $node->get($field_name)->getFieldDefinition();
      $items = ($fieldDefinition->getType() === 'entity_reference') ? $node->get($field_name)
        ->referencedEntities() : $node->get($field_name)->getValue();

      foreach ($items as $values) {
        $output_values = array_merge($output_values, $this->extractValue($values, $fieldDefinition));
      }

      if (count($output_values)) {
        $output['label'] = ($displayFields[$field_name]['label'] !== 'hidden') ? $fieldDefinition->getLabel() : '';
        $output['values'] = $output_values;
      }
    }

    return $output;
  }

  /**
   * Extracts values of a field.
   *
   * @param object|array $values
   *   Array of values.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   Fieldobject.
   *
   * @return array
   *   Extracted data.
   */
  private function extractValue($values, FieldDefinitionInterface $fieldDefinition) {
    $output = [];
    switch ($fieldDefinition->getType()) {
      case 'entity_reference':
        $output[] = $values->label();
        break;

      case 'datetime':
        $output[] = date('d.m.Y', strtotime($values['value']));
        break;

      case 'daterange':
        $output[] = date('d.m.Y', strtotime($values['value'])) . ' - ' . date('d.m.Y', strtotime($values['end_value']));
        break;

      case 'list_integer':
        $field = $fieldDefinition->getFieldStorageDefinition();
        $options = options_allowed_values($field);
        $output[] = $options[$values['value']];
        break;

      default:
        if (!empty($values['value'])) {
          $output[] = $values['value'];
        }
    }
    return $output;
  }

}
