<?php

namespace Drupal\pdf;

use Drupal\node\Entity\Node;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\SimpleType\Jc;
use PhpOffice\PhpWord\Style\Image;
use PhpOffice\PhpWord\Shared\Converter;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Settings;

/**
 * Class for PdfGenerator.
 */
class PdfGenerator {

  /**
   * Generates the base of pdf as word document.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Node for PDF creation.
   *
   * @return string
   *   returns the Pdf path.
   *
   * @throws \PhpOffice\PhpWord\Exception\Exception
   */
  public static function generate(Node $node) {
    Settings::setCompatibility(TRUE);
    Settings::setOutputEscapingEnabled(TRUE);
    $rendererName = Settings::PDF_RENDERER_DOMPDF;
    $rendererLibraryPath = realpath(DRUPAL_ROOT . '/../vendor/dompdf/dompdf');
    Settings::setPdfRenderer($rendererName, $rendererLibraryPath);

    $document = new PhpWord();
    $extractor = new PdfExtractor();
    $extract = $extractor->extract($node);
    $section = $document->addSection();
    $header = $section->addHeader();
    $footer = $section->addFooter();

    $logo = [
      'positioning' => Image::POSITION_ABSOLUTE,
      'posHorizontal' => Image::POSITION_HORIZONTAL_RIGHT,
      'posVertical' => Image::POSITION_VERTICAL_BOTTOM,
      'posHorizontalRel' => Image::POSITION_RELATIVE_TO_PAGE,
      'posVerticalRel' => Image::POSITION_RELATIVE_TO_PAGE,
      'marginLeft' => Converter::cmToPixel(24),
    ];

    $header->addText($extract['label'], ['size' => 18]);
    $footer->addPreserveText('Seite {PAGE} von {NUMPAGES}', NULL, ['alignment' => Jc::START]);
    $footer->addImage(\Drupal::service('extension.list.module')->getPath('pdf') . '/logo.png', $logo);

    foreach ($extract['values'] as $group) {
      $section->addText($group['label'], ['size' => 16]);

      foreach ($group['values'] as $field) {
        if (strlen($field['label'])) {
          $section->addText($field['label'] . ': ', ['bold' => TRUE]);
        }

        foreach ($field['values'] as $value) {
          $section->addText($value);
        }

        $section->addTextBreak();
      }
    }
    $objWriter = IOFactory::createWriter($document);
    $objWriter->save('/tmp/' . $node->id() . '.docx');
    $cmd = escapeshellcmd('libreoffice --headless --convert-to pdf --outdir /tmp/') . ' ' . escapeshellarg('/tmp/' . $node->id() . '.docx');
    shell_exec($cmd);
    return 'temporary://' . $node->id() . '.pdf';
  }

}
